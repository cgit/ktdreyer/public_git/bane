%global commit f7282b890afd78656ffa71355a3f223e07be04e6
%global shortcommit %(c=%{commit}; echo ${c:0:7})

%global         checkout 20130203git%{shortcommit}

Name:           bane
Version:        0.4.0
Release:        1.%{checkout}%{?dist}
Summary:        (Yet another) Event emitter for Node, Browser globals and AMD
Group:          Applications/Internet
License:        BSD
URL:            https://gitorious.org/buster/bane
# To download the source tarball:
# curl -o bane-%{shortcommit}.tar.gz https://gitorious.org/buster/bane/archive-tarball/%{commit}
# You may get a "try again later" message at first.
Source0:        %{name}-%{shortcommit}.tar.gz
Source1:        %{name}.conf
BuildArch:      noarch

#BuildRequires:  
Requires:       httpd

%description
(Yet another) Event emitter for Node, Browser globals and AMD


%prep
%setup -q -n buster-%{name}

%build

%install
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -p buster.js $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -p -r lib  $RPM_BUILD_ROOT%{_datadir}/%{name}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d
install -p -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d

%files
%doc AUTHORS LICENSE Readme.rst
%{_datadir}/%{name}
%config(noreplace) %{_sysconfdir}/httpd/conf.d/%{name}.conf

%changelog
* Mon Feb 04 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.4.0-1.20130204git
- Initial packaging
